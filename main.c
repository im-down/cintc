#include <stdio.h>
#include <math.h>

int main() {
	double principal, compoundPerYear, time, interestRate, amount;
	
	printf("compounding interest calculator by twl\n\n");
	printf("input the principal:\n(amount of money invested)\n");
	scanf("%lf", &principal);
	printf("input the interest rate in decimal:\n(1.00% = 0.0100)\n");
	scanf("%lf", &interestRate);
	printf("input the amount of years to calculate\n");
	scanf("%lf", &time);
	printf("imput the amount of times your investment compounds per year\n");
	scanf("%lf", &compoundPerYear);
	// the actual calculation
	double math  = (1.0+(interestRate / compoundPerYear));
	math = pow(math, (compoundPerYear * time));
	amount = principal * math;
	
	
	
	
	printf("\n\nyour $%lf principal will compound with a %lf interest rate in %lf year(s) to:\n $%lf\n\n", principal, interestRate, time, amount);
	return 0;
}
// compile with $ gcc main.c -lm
